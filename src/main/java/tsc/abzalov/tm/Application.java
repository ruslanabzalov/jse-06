package tsc.abzalov.tm;

import java.util.Scanner;

import static tsc.abzalov.tm.constant.ApplicationCommand.*;

/**
 * Основной класс приложения.
 * @author Ruslan Abzalov.
 */
public class Application {

    /**
     * Основной метод приложения.
     * @param args Аргументы командной строки.
     */
    public static void main(String... args) {
        displayWelcomeMessage();

        if (areArgsExist(args)) return;

        final Scanner scanner = new Scanner(System.in);
        String command;
        while (true) {
            System.out.print("Please, Enter Your Command: ");
            command = scanner.nextLine();
            System.out.println();
            parseArg(command);
        }
    }

    /**
     * Метод, отображающий приветственное сообщение.
     */
    private static void displayWelcomeMessage() {
        System.out.println("****** WELCOME TO TASK MANAGER APPLICATION ******\n");
    }

    /**
     * Метод, проверяющий наличие аргументов командной строки.
     * @param args Аргументы командной строки.
     */
    private static boolean areArgsExist(String... args) {
        if (args == null || args.length == 0) {
            System.out.println("Application arguments are empty!\n" +
                               "Please, re-run the application with \"help\" " +
                               "command to see available commands.\n");
            return false;
        }

        parseArg(args[0]);
        return true;
    }

    /**
     * Метод, обрабатывающий аргумент командной строки.
     * @param arg Аргумент командной строки.
     */
    private static void parseArg(String arg) {
        if (arg == null) return;

        switch (arg) {
            case CMD_HELP:
                showHelp();
                break;
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_EXIT:
                exit();
                break;
            default:
                System.out.println("Command \"" + arg + "\" is not available!\n" +
                                   "Please, re-run the application with available input command.\n");
        }
    }

    /**
     * Метод, отображающий доступные команды приложения.
     */
    private static void showHelp() {
        System.out.println("Available Commands:\n" +
                           "help: Displays all available commands.\n" +
                           "version: Displays application version.\n" +
                           "about: Displays developer info.\n" +
                           "exit: Exit Application.\n");
    }

    /**
     * Метод, отображающий версию приложения.
     */
    private static void showVersion() {
        System.out.println("Version: 1.0.0\n");
    }

    /**
     * Метод, отображающий основную информацию о разработчике.
     */
    private static void showAbout() {
        System.out.println("Developer Full Name: Ruslan Abzalov\n" +
                           "Developer Email: rabzalov@tsconsulting.com\n");
    }

    /**
     * Метод выхода из приложения.
     */
    private static void exit() {
        System.out.println("Application Is Closing...");
        System.exit(0);
    }

}
