package tsc.abzalov.tm;

import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApplicationTest {

    @Test
    @DisplayName("Тест с аргументами командной строки")
    @Order(1)
    void argsTest() {
        System.out.println("################### Help Test ###################");
        Application.main("help");
        System.out.println();

        System.out.println("################### Version Test ###################");
        Application.main("version");
        System.out.println();

        System.out.println("################### About Test ###################");
        Application.main("about");
        System.out.println();

        System.out.println("################### Incorrect Argument Test ###################");
        Application.main("someCommand");
        System.out.println();
    }

}